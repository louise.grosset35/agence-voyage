<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.18.0/font/bootstrap-icons.css" rel="stylesheet">
</head>
<body class="bg-grey">

<?php include 'config.php'; $email = $_GET['email']; ?>

<div class="mt-3 row justify-content-center">
        <div class="btn-retour">
        <a href="circuit_show.php?email=<?php echo $email; ?>" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
        </div>
        <div class="btn-retour-tel">
        <a href="circuit_show.php?email=<?php echo $email; ?>" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
        </div>
    </div>
<body>




    <?php
        include 'config.php';
        
        if (isset($_GET['id']))  {
            $id_choisi = $_GET['id'];
            $requete_sql = "SELECT * FROM reservation WHERE id_reservation = :id; ";
            $sth = $conexion_bd->prepare($requete_sql);
            $data = ['id' => $id_choisi];
            $is_successful = $sth->execute($data);  
            $resa = $sth->fetchAll();
            
            echo "<div class='container mt-5'>
                    <div class='row justify-content-center'>
                        <div class = 'container mt-5'>
                            <form action='#' method='post'>
                            <h2 class='text-center bg-light text-wrap around text-uppercase fw-semibold'>Ajuster les modifications</h2>
                
                                <div class='mb-3'>
                                    <label for='nom'>Date :</label>
                                    <input type='date' name='date' id='date'  value='".$resa[0][1]."'>
                                </div>
                                <div class='mb-3'>
                                    <label for='description'>Nombre de places :</label>
                                    <input type='number' name='nb_place' id='nb_place'  value='".$resa[0][3]."'>
                                </div>
                                <div class='mb-3'>
                                    <label for='statue'>Choisir une option:</label>
                                </div>
                                <div class='mb-3'>
                                    <select name='statue' id='statue'>
                                    <option value=''></option>
                                    <option value='En cours'>En cours</option>
                                    <option value='Validé'>Validé</option>
                                    <option value='Annulé'>Annulé</option>
                                    </select>
                                </div>

                                <div class='mb-3 text-center'>
                                <input type='submit' class='btn btn-dark' value='Valider' name='confirmation'>
                            </div>
                            </form>
                        </div>
                        </div></div>";
                
            if (isset($_POST['confirmation'])){
                echo"<h1 class = 'titre'>votre réservations a été modifier avec succès </h1>";
            
            
            $table = ['date','nb_place','statue'];
            $modif = [];
            for ($i = 0; $i <= 2; $i++){
                $valeur =  $_POST[$table[$i]]??$circuit[$i];
                $modif[$i] = $valeur;
            }
            

            $requete_sql = "UPDATE reservation 
            SET reservation.date = :nvdate,
                nb_place = $modif[1],
                statue = :nvstatue
            WHERE id_reservation = $id_choisi;";

            $data = [
                'nvdate'=>strval($modif[0]),
                'nvstatue'=>strval($modif[2])];


            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data);


                
    }}?>

</body>
</html>