<!--
#connection à la base de donnée admin/user
    si user :
        voir si son mdp correspont avec des requete post en comparant dans la bdd 
        
        posibilité de créer un compte et donc que le remplissage se fasse dans la bdd 
        pour la creation verifier que l'email est pas deja utiliser 
        
    créer un bouton connection admin :
        se connceter normalement avec la meme verification 
-->
<!DOCTYPE html>

<html lang="fr">
<!-- ouvrir le site depuit le terminal php -S localhost: -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body class="bg-index">
    <div class="d-grid gap-2 col-6 mx-auto center-div pt15">
        <a href="connection_new_user.php"  class="btn btn-primary btn-lg">Créer un compte</a>
        <a href="connection_user.php"  class="btn btn-primary btn-lg">Se connecter</a><br>
        <a href="connection_admin.php"  class="btn btn-secondary btn-lg">Se connecter en tant qu'admin </a>
    </div>
</body>
</html>
