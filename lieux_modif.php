<!DOCTYPE html>

<html lang="fr">
<!-- ouvrir le site depuit le terminal php -S localhost: -->

<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.18.0/font/bootstrap-icons.css" rel="stylesheet">
</head>

<body class="bg-grey">

    <?php include 'config.php'; $email = $_GET['email']; ?>

    <div class="mt-3 row justify-content-center">
        <div class="col-md-4 text-center">
            <a href="circuit_show.php?email=<?= $email ?>" class="btn btn-warning btn-lg mb-2">Voir tous les circuits</a>
        </div>
        <div class="col-md-4 text-center">
            <a href="lieux_create.php?email=<?= $email ?>" class="btn btn-warning btn-lg mb-2">Créer un lieu</a>
        </div>
        <div class="col-md-4 text-center">
            <a href="lieux_show.php?email=<?= $email ?>" class="btn btn-warning btn-lg mb-2">Voir les lieux</a>
        </div>
    </div>

    <?php
        include 'config.php';
        
        
        if (isset($_GET['id']))  {
            $id_choisi = $_GET['id'];
            $requete_sql = "SELECT * FROM lieux WHERE idLieux = :id; ";
            $sth = $conexion_bd->prepare($requete_sql);
            $data = ['id' => $id_choisi];
            $is_successful = $sth->execute($data);  
             
            $lieux = $sth->fetchAll();



           
             echo '<div class="container mt-5">
                    <div class="row justify-content-center">
                        <div class="col-md-6">
                            <form action="#" method="post">
                                <h2 class="text-center bg-light text-wrap around text-uppercase fw-semibold">Ajuster les modifications</h2>
            
                                <div class="mb-3">
                                    <label for="nom" class="form-label">Nom du lieu :</label>
                                    <input type="text" class="form-control" id="nom" name="nom" value="' . $lieux[0][1] . '">
                                </div>
            
                                <div class="mb-3">
                                    <label for="description" class="form-label">Description :</label>
                                    <input type="text" class="form-control" id="description" name="description" value="' . $lieux[0][3] . '">
                                </div>
            
                                <div class="mb-3">
                                    <label for="prix" class="form-label">Coût :</label>
                                    <input type="number" class="form-control" id="prix" name="prix" value="' . $lieux[0][4] . '">
                                </div>
            
                                <div class="mb-3">
                                    <label for="duree" class="form-label">Durée :</label>
                                    <input type="time" class="form-control" id="duree" name="duree" value="' . $lieux[0][5] . '">
                                </div>
            
                                <div class="mb-3">
                                    <label for="id_ville" class="form-label">Ville :</label>
                                    <input type="number" class="form-control" id="id_ville" name="id_ville" value="' . $lieux[0][2] . '">
                                </div>
            
                                <div class="mb-3">
                                    <label for="photo" class="form-label">Photo :</label>
                                    <input type="text" class="form-control" id="photo" name="photo" value="' . $lieux[0][6] . '">
                                </div>
            
                                <div class="mb-3 text-center">
                                    <input type="submit" class="btn btn-dark" value="Modifier ce lieu" name="confirmation">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>';
        
            
            if (isset($_POST['confirmation'])){
                echo"<br><br><h3 class = 'text-center black'>Votre circuit a été modifié avec succès </h3><br><br>";
            
            
            $table = ['nom','id_ville','description','prix','duree','photo'];
            $modif = [];
            for ($i = 0; $i <= 5; $i++){
                $valeur =  $_POST[$table[$i]]??$circuit[$i];
                $modif[$i] = $valeur;
            }
            

            $requete_sql = "UPDATE lieux 
            SET label = :nvnom,
            id_ville = $modif[1],

            duree = :duree,
            prix_visite = $modif[3],
            lieux.description = :nv_desc,
            photos = :nv_photo
            WHERE idLieux = $id_choisi";

            $data = [
                'nvnom'=>$modif[0],
                'nv_desc' => $modif[2],
                'nv_photo' => $modif[5],
                'duree'=> strval($modif[4])];


            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data);


                
    }}?>

</body>
</html>