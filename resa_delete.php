<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.18.0/font/bootstrap-icons.css" rel="stylesheet">
</head>
<body class="bg-grey">

<?php include 'config.php'; $email = $_GET['email']; ?>

<div class="mt-3 row justify-content-center">
        <div class="btn-retour">
        <a href="circuit_show.php?email=<?php echo $email; ?>" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
        </div>
        <div class="btn-retour-tel">
        <a href="circuit_show.php?email=<?php echo $email; ?>" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
        </div>
    </div>

<div class='center-div mt-3'>
        <form action='#' method='post'>
            <h2 class='text-center bg-light text-wrap around text-uppercase fw-semibold'>SUPPRESSION RESERVATION</h2>

            <div class="mb-3">
                <label for="delete" class="form-label">Tapez OUI si vous souhaitez réellement la supprimer totalement :</label>
                <input type="text" class="form-control" id="delete" name="delete" value="OUI/NON">
            </div>

            <input type="submit" class="btn btn-danger" value="Supprimer réservation">        
        </form>
    </div>

    <?php
        include 'config.php';
        $id_lieux = $_GET['id'];
        $email = $_GET['email'];
        
        if (isset($_POST['delete']))  
        {
            $reponse = $_POST['delete'];

        
        if ($reponse == 'OUI'){
            
           
            $requete_delete_1 = " DELETE  FROM `reservation` WHERE id_reservation = :id_resa";
            $data = ['id_resa'=>$id_lieux];
            $sth = $conexion_bd->prepare($requete_delete_1);
            $is_successful = $sth->execute($data); 




            if ($is_successful){
                echo 'votre circuit a bien été suprimer avec toute ses réservations.';

        }
    }
    echo '<a href="circuit_show.php&email=$email"  class="boutons">voir l"ensemble des circuit</a>';}


    ?>
</body>
</html>