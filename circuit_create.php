<!DOCTYPE html>

<html lang="fr">
<!-- ouvrir le site depuit le terminal php -S localhost: -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.18.0/font/bootstrap-icons.css" rel="stylesheet">
</head>

<body class='bg-grey'>

    <?php include 'config.php'; $email = $_GET['email']; ?>

    <div class="btn-retour">
         <a href="circuit_show.php?email=<?php echo $email; ?>" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
    </div>
    <div class="btn-retour-tel">
        <a href="circuit_show.php?email=<?php echo $email; ?>" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
    </div>

    <div class="container mt-5">
        <form action="#" method="get">
            <h2 class='text-center bg-light text-wrap around text-uppercase fw-semibold'>Renseignez les informations</h2>
            <br /><br />

            <div class="mb-3">
                <label for="nom" class="form-label">Nom:</label>
                <input type="text" class="form-control" id="nom" name="nom" value="">
            </div>

            <div class="mb-3">
                <label for="ville_D" class="form-label">Ville de Départ:</label>
                <input type="text" class="form-control" id="ville_D" name="ville_D" value="">
            </div>

            <div class="mb-3">
                <label for="ville_A" class="form-label">Ville d'Arrivée:</label>
                <input type="text" class="form-control" id="ville_A" name="ville_A" value="">
            </div>

            <div class="mb-3">
                <label for="nb_etapes" class="form-label">Nombre d'étapes:</label>
                <input type="number" class="form-control" id="nb_etapes" name="nb_etapes" value="">
            </div>

            <div class="mb-3">
                <label for="duree" class="form-label">Durée:</label>
                <input type="number" class="form-control" id="duree" name="duree" value="">
            </div>

            <div class="mb-3">
                <label for="prix" class="form-label">Prix d'inscription:</label>
                <input type="number" class="form-control" id="prix" name="prix" value="">
            </div>

            <div class="mb-3">
                <label for="desc" class="form-label">Description:</label>
                <input type="text" class="form-control" id="desc" name="desc" value="">
            </div>

            <input type="submit" class="btn btn-dark" value="Enregistrer ce circuit">
        </form>
    </div>
    <br /><br />



                        

    <?php
        include 'config.php';
        $email = $_GET['email'];



        $requete_sql = "SELECT user.admin FROM `user` 
        WHERE adresse_mail = :email ;";


                                        
            $data = ['email'=>$email];
            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data); 
            $test_admin = $sth->fetchAll();


        // if ($test_admin !=[] and $test_admin[0][0] == 1){
        //     echo '
        //     <div class="bouton">
        //     <a href="circuit_create.php?email=$email"  class="boutons">créer un circuit</a>
        //     <a href="lieux_show.php?email=$email"  class="boutons">voir les lieux</a>
        //     <a href="circuit_show.php?email=$email"  class="boutons">voir l"ensemble des circuit</a>

        // </div>';
        

        if (isset($_GET['nom']))  
        {
            $new_nom = $_GET['nom'];
            $new_desc = $_GET['desc'];
            $new_cout = $_GET['prix'];
            $new_ville_A = $_GET['ville_A'];
            $new_ville_D = $_GET['ville_D'];
            $new_nb_etapes = $_GET['nb_etapes'];
            $new_duree = $_GET['duree'];
            
            
            $requete_sql = "SELECT id_ville FROM `ville` 
                            WHERE nom = :ville";
            $data = ['ville'=>$new_ville_A];
            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data); 
            $id_ville_A = $sth->fetch();
    
            
            $requete_sql = "SELECT id_ville FROM `ville` 
            WHERE nom = :ville";
            $data = ['ville'=>$new_ville_D];
            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data); 
            $id_ville_D = $sth->fetch();
            
    
        
            $requete = "SELECT MAX(idCircuit)+1 FROM circuit";
            $sth = $conexion_bd->prepare($requete);
            $data = [];
            $is_successful = $sth->execute($data); 
            $new_id = $sth->fetch();

            if ($id_ville_A == [] or $id_ville_D == []){
                echo'Veuillez selectionner une ville valide'; 
            }
            else {
            $new_circuit = "INSERT INTO circuit VALUES (:nv_id,
                                                        :nv_nom,
                                                        :nv_ville_D,
                                                        :nv_ville_A,
                                                        :nv_nb_etapes,
                                                        :nv_duree,
                                                        :nv_cout,
                                                        :nv_nb_jaime,
                                                        :nv_desc)";
                                                            
            $data = [
                'nv_id'=>$new_id[0],
                'nv_nom'=>$new_nom,
                'nv_ville_D' => $id_ville_D[0],
                'nv_ville_A' => $id_ville_A[0],
                'nv_nb_etapes' => intval($new_nb_etapes),
                'nv_duree' => intval($new_duree),
                'nv_cout' => intval($new_cout),
                'nv_nb_jaime' => 0,
                'nv_desc' => $new_desc 
            ];
            $sth = $conexion_bd->prepare($new_circuit);
            $is_successful = $sth->execute($data);
            echo 'super';

        }}
        if (isset($new_nb_etapes) and $id_ville_A != [] and $id_ville_D != []){
            echo'<form action="#" method="post">';

        for ($i = 1; $i <= intval($new_nb_etapes); $i++) {
            echo '   lieux   ' ,$i ;
        
        ?>

    <select  id='lieux'  name='lieux'>
    <br /><br />
    
    <option selected="selected">Sélectionnez un lieux</option>

      <?php

      foreach($liste_lieux as $lieu){
      ?>
      <option value="<?php echo strtolower($lieu[0]); ?>"><?php echo $lieu[0]; ?></option>
      <?php
      }
      echo'</select>';

      
    }
    echo '<input type="submit" class="boutons"value="Valider ce lieux "></form>';
 


    if (isset($_POST['lieux'])) {


    $lieux = $_POST['lieux'];


 
    $requete_sql = "SELECT idLieux FROM lieux
                    WHERE label = :nom ";
        $data = ['nom'=>$lieux,];
        $sth = $conexion_bd->prepare($requete_sql);
        $is_successful = $sth->execute($data); 
        $id_lieux = $sth->fetch();
    
    $requete_sql = "INSERT INTO lieux_has_circuit 
                    VALUES (:id_lieux,:id_circuit, :ordre)";
        $data = [ 'id_lieux'=>$id_lieux[0],
                'id_circuit' => $new_id[0],
                    'ordre' => $i];
    
        $sth = $conexion_bd->prepare($requete_sql);
        $is_successful = $sth->execute($data); ;}}



    ?>



</body>
</html>