<!DOCTYPE html>

<html lang="fr">
<!-- ouvrir le site depuit le terminal php -S localhost: -->

<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body class="bg-index">
    
    <div class="btn-retour">
        <a href="index.php" class="btn btn-light btn-lg btn-outline-dark">Retour</a>
    </div>
    <div class="btn-retour-tel">
        <a href="index.php" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
    </div>

    <div class="center-div">
         <form action='#' method='post'>
            <h2 class='text-center bg-light text-wrap around text-uppercase fw-semibold'>Création de compte</h2>
                <br />
                <div>
                    <div class =''>
                        <label for='atribut' class="fs-5 fw-semibold white">Nom:</label>
                        <input type='text' class="form-control-sm"  name='nom' id='nom' >
                         <br>
                     </div>
                     <div class =''>
                        <label for='atribut' class="fs-5 fw-semibold white">Prénom:</label>
                        <input type='text' class="form-control-sm" name='prenom' id='prenom' >
                        <br>
                    </div>
                    <div class =''>
                        <label for='atribut' class="fs-5 fw-semibold white">Adresse mail:</label>
                        <input type='text'class="form-control-sm"  name='email' id='email' >
                        <br>
                    </div>
                    <div class =''>
                        <label for='atribut' class="fs-5 fw-semibold white">Pays:</label>
                        <input type='text'class="form-control-sm"  name='pays' id='pays' >
                        <br>
                    </div>
                    <div class =''>
                        <label for='atribut' class="fs-5 fw-semibold white">Ville:</label>
                        <input type='text' class="form-control-sm" name='ville' id='ville' >
                        <br>
                    </div>
                    <div class =''>
                        <label for='atribut' class="fs-5 fw-semibold white">Mot de Passe:</label>
                        <input type='text'class="form-control-sm"  name='mdp' id='mdp' >
                        <br><br><br>
                    </div>
                </div>
                <div class="ok">
                    <input type='submit' class="btn bg-light btn-outline-dark" value='Enregistrer ce compte'>
                </div>
        </form>
    </div>
                
    <?php
        include 'config.php';
        if (isset($_POST['email']))  {
            $email = $_POST['email'];
            $mdp = $_POST['mdp'];
            $prenom = $_POST['prenom'];
            $nom = $_POST['nom'];
            $pays = $_POST['pays'];
            $ville = $_POST['ville'];

            $requete_sql = "SELECT id FROM `payss` 
                            WHERE nom_fr_fr = :pays";
            $data = ['pays'=>$pays];
            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data); 
            $id_pays = $sth->fetch();
            
            
            if ($id_pays == false){
                $requete_sql = "SELECT max(id)
                                    FROM payss ";
                $data = [];
                $sth = $conexion_bd->prepare($requete_sql);
                $is_successful = $sth->execute($data); 
                $id_pays = $sth->fetch();
                $id_pays = $id_pays[0] + 1;
                
                
                $requete_sql = "INSERT INTO `payss` (`id`,`nom_fr_fr`) VALUES 
                                            (:id_pays, :nom);";
                $data = ['nom'=>$pays,
                        'id_pays'=>$id_pays];

                $sth = $conexion_bd->prepare($requete_sql);
                $is_successful = $sth->execute($data); }


            
            
            $requete_sql = "SELECT id_ville FROM `ville` 
                            WHERE nom = :ville";
            $data = ['ville'=>$ville];
            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data); 
            $id_ville = $sth->fetch();




            
            if ($id_ville == false){
                $requete_sql = "SELECT max(id_ville)
                                FROM ville ";
                $data = [];
                $sth = $conexion_bd->prepare($requete_sql);
                $is_successful = $sth->execute($data); 
                $id_ville = $sth->fetch();
                $id_ville = $id_ville[0] + 1;
                                
                            
            
                $requete_sql = "INSERT INTO `ville` (`id_ville`, `nom`, `id_pays`) VALUES 
                                            (:id,:ville, :id_pays);";
                $data = ['id'=>$id_ville,
                        'ville'=>$ville,
                        'id_pays'=>$id_pays];

                $sth = $conexion_bd->prepare($requete_sql);
                $is_successful = $sth->execute($data); 
                
                
                $requete_sql = "SELECT id_ville FROM `ville` 
                                WHERE ville.nom = :ville";
                $data = ['ville'=>$ville];
                $sth = $conexion_bd->prepare($requete_sql);
                $is_successful = $sth->execute($data); 
                $id_ville = $sth->fetch();
            }

            print_r($id_ville[0]);
            print_r($id_pays[0]);                
            
            $requete_sql = "SELECT max(id_client)
                            FROM user ";
            $data = [];
            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data); 
            $id_clients = $sth->fetch();
            $id_client = $id_clients[0] + 1;
            
            $requete_sql = "INSERT INTO user
                            VALUES 
                            (:id,:nom, :prenom, :email, :id_pays, :mdp, :id_ville, :dmin)";

            $data = [   
                    'id' => $id_client,
                    'nom'=>$nom,
                    'prenom' => $prenom,
                    'email' => $email,
                    'id_pays' => $id_pays[0],
                    'mdp' => $mdp,
                    'id_ville' => $id_ville[0],
                    'dmin' => 0 ];
            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data); 
            echo "
            <div class='center-div'><h3 class = 'white'> Votre compte a bien été créé !</h3></div>";}
        ?>
    
    </body>
    </html>


</body>
</html>