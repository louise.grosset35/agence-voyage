<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.18.0/font/bootstrap-icons.css" rel="stylesheet">
</head>

<body class='bg-grey'>

    <?php include 'config.php'; $email = $_GET['email']; $id_circuit = $_GET['id']; ?>
    
<div class="mt-2 row justify-content-between">
    <div class="d-flex">
        <div class="btn-retour">
            <a href="circuit_show.php?email=<?php echo $email; ?>" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
        </div>
        <div class="btn-retour-tel">
            <a href="circuit_show.php?email=<?php echo $email; ?>" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
        </div>
    </div>
    <div class="btn">
        <a href="circuit_delete.php?id=<?php echo $id_circuit; ?>&email=<?php echo $email; ?>" class="btn btn-danger btn-outline-dark fw-semibold">Supprimer ce circuit</a>
    </div>
</div>

    
    </div><br><br>
    <?php
        include 'config.php';
        $email = $_GET['email'];
        $id_circuit = $_GET['id'];


        $requete_sql = "SELECT user.admin FROM `user` 
        WHERE adresse_mail = :email ;";                               
        $data = ['email'=>$email];
        $sth = $conexion_bd->prepare($requete_sql);
        $is_successful = $sth->execute($data); 
        $test_admin = $sth->fetchAll();

        $requete_circuit = "SELECT * FROM `circuit` 
                        WHERE idCircuit = :id ;";
        $data = ['id'=> $id_circuit];
        $sth = $conexion_bd->prepare($requete_circuit);
        $is_successful = $sth->execute($data); 
        $result = $sth->fetchAll();

        $requete_ville_D = "SELECT nom FROM `ville` 
                        WHERE id_ville = :id ;";
        $data = ['id'=>$result[0][2]];
        $sth = $conexion_bd->prepare($requete_ville_D);
        $is_successful = $sth->execute($data); 
        $nom_ville_D = $sth->fetchAll();

        $requete_ville_A = "SELECT nom FROM `ville` 
        WHERE id_ville = :id ;";
        $data = ['id'=>$result[0][3]];
        $sth = $conexion_bd->prepare($requete_ville_A);
        $is_successful = $sth->execute($data); 
        $nom_ville_A = $sth->fetchAll();

        $requete_resa = "SELECT * FROM `reservation` WHERE id_circuit = :id_circuit";
        $data = ['id_circuit'=>$result[0][0]];
        $sth = $conexion_bd->prepare($requete_resa);
        $is_successful = $sth->execute($data); 
        $resa = $sth->fetchAll();

        $requete_lieu = "SELECT Lieux_idLieux FROM `lieux_has_circuit` 
                        WHERE Circuit_idCircuit = :id_circuit
                        ORDER BY `lieux_has_circuit`.`ordre` ASC";
        $data = ['id_circuit'=>$result[0][0]];
        $sth = $conexion_bd->prepare($requete_lieu);
        $is_successful = $sth->execute($data); 
        $etapes = $sth->fetchAll();

        $id = $result[0][0];
        $name = $result[0][1];
        $description = $result[0][8];
        $aime = $result[0][4];
        $ville_D = $nom_ville_D[0][0];
        $ville_A = $nom_ville_A[0][0];
        $duree = $result[0][5];
        $prix = $result[0][6];
        $photos = $result[0][9];
        $nb_etapes = $result[0][4];
        
        echo "<div class='col-md-8 mx-auto center-div '>
                <div class='card border-warning'>
                    <img src='$photos' class='card-img-top' alt='Photos du circuit'>
                    <div class='card-body'>
                        <h5 class='card-title'>$name</h5><br>
                        <p class='card-text'>$description</p>
                        <p class='card-text'>Nombre de j'aime : $aime</p>
                        <p class='card-text'>Ville de départ : $ville_D</p>
                        <p class='card-text'>Ville d'arrivée : $ville_A</p>
                        <p class='card-text'>Nombre d'étapes : $nb_etapes</p>
                        <p class='card-text'>Durée : $duree</p>
                        <p class='card-text'>Prix : $prix</p>
                    </div>
                </div>
            </div>";

                             
// if ($test_admin !=[] and $test_admin[0][0] == 1){
//             echo "
//             <div class='bouton'>
//             <a href='circuit_create.php?email=$email'  class='boutons'>créer un circuit</a>
//             <a href='lieux_show.php?email=$email'  class='boutons'>voir les lieux</a>
//             <a href='circuit_show.php?email=$email'  class='boutons'>voir l'ensemble des circuit</a>

//         </div>";
//         }
                    

echo "<h3 class='text-center pt3'> Les reservations disponibles : </h3><br>";
for ($i = 1; $i <= count($resa); $i++) {
    $id_resa = $resa[$i-1][0];
    $date = $resa[$i-1][1];
    $nb_place = $resa[$i-1][3];
    $statue = $resa[$i-1][4];
            
        


    echo "<div class='col-md-8 mx-auto center-div '>
            <div class='card border-warning'>
                <div class='card-body'>
                    <h5 class='text-center'>Réservation $i :</h5><br>
                    <p class='card-text'>Date : $date</p>
                    <p class='card-text'>Nombre de places : $nb_place</p>
                    <p class='card-text'>Statut : $statue</p>
                    <a href='resa_modif.php?id=$id_resa&email=$email' class='btn btn-dark'>Modifier cette réservation</a>
                    <a href='resa_delete.php?id=$id_resa&email=$email' class='btn btn-danger'>Supprimer cette réservation</a>
                </div><br><br>
                <a href='resa_create.php?id=$id_resa&email=$email' class='btn btn-success'>Ajouter une réservation</a>
                </div>
        </div>";
}

                 


echo "<h3 class='text-center pt-3'>Les étapes disponibles :</h3><br>";

for ($i = 1; $i <= intval($nb_etapes); $i++) {
    echo "<div class='col-md-5 mx-auto mb-4'>
            <div class='card border-warning'>
                <div class='card-body d-flex flex-column'>
                    <h5 class='card-title text-center'>Étape $i :</h5><br>";

    $requete_sql = "SELECT * FROM `lieux`
                    WHERE idLieux = :id_lieux;";
    $data = ['id_lieux' => $etapes[$i - 1][0]];
    $sth = $conexion_bd->prepare($requete_sql);
    $is_successful = $sth->execute($data);
    $lieux = $sth->fetchAll();

    $id = $lieux[0][0];
    $nom = $lieux[0][1];
    $description = $lieux[0][3];
    $ville = $lieux[0][2];
    $prix = $lieux[0][4];
    $photos = $lieux[0][6];

            echo "<p class='card-text text-center'>$nom</p>
                <p class='card-text text-center'>$description</p>
                <p class='card-text text-center'>Ville de départ : $ville</p>
                <p class='card-text text-center'>Prix : $prix</p>
                <p class='card-text text-center'>$photos</p>
                <a href='lieux_modif.php?id=$id&email=$email' class='btn btn-dark mt-auto'>Modifier le lieu</a>
                </div>
            </div>
         </div>";
}


    ?>
</body>
</html>