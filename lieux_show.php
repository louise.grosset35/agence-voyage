<!DOCTYPE html>

<html lang="fr">
<!-- ouvrir le site depuit le terminal php -S localhost: -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.18.0/font/bootstrap-icons.css" rel="stylesheet">
</head>

<body class="bg-grey">


    <?php
            include 'config.php';
            $email = $_GET['email'];


        $requete_sql = "SELECT user.admin FROM `user` 
        WHERE adresse_mail = :email ;";
    
    
                                            
                $data = ['email'=>$email];
                $sth = $conexion_bd->prepare($requete_sql);
                $is_successful = $sth->execute($data); 
                $test_admin = $sth->fetchAll();

        if ($test_admin !=[] and $test_admin[0][0] == 1){   
            echo "
            <div class='mt-3 row justify-content-center'>
                <div class='col-md-4 text-center'>
                    <a href='circuit_show.php?email=<?= $email ?>' class='btn btn-warning btn-lg mb-2'>Voir tous les circuits</a>
                </div>
                <div class='col-md-4 text-center'>
                    <a href='lieux_create.php?email=$email'  class='btn btn-warning btn-lg mb-2'>Ajouter un nouveau lieu</a>
                </div>
            </div>";
        }

        include 'config.php';

        $requete_sql = "SELECT *, nom FROM lieux JOIN ville ON lieux.id_ville = ville.id_ville";

        $data = [];
        $sth = $conexion_bd->prepare($requete_sql);
        $is_successful = $sth->execute($data);
        $Lieux = $sth->fetchAll();




        foreach ($Lieux as $Lieu) {
            echo "<div class='row pt-3 center-div'>
                    <div class='col-sm-6 mb-3 mb-sm-0'>
                        <div class='card'>
                            <div class='card-body'>
                                <h5 class='card-title'>$Lieu[label]</h5>
                                <img src='$Lieu[photos]' class='img_index' alt='Image du lieu'>
                                <p class='card-text'>Ville: $Lieu[nom]</p>
                                <p class='card-text'>Prix visite: $Lieu[prix_visite]</p>
                                <p class='card-text'>Description: $Lieu[description]</p>
                                <p class='card-text'>Durée: $Lieu[duree]</p>
                                <a href='lieux_modif.php?id=$Lieu[idLieux]&email=$email' class='btn btn-warning'>Modifier ce lieu</a>
                                <a href='lieux_delete.php?id=$Lieu[idLieux]&email=$email' class='btn btn-danger'>Supprimer ce lieu</a>
                            </div>
                        </div>
                    </div>
                  </div>
                  <br><br>";
        }
        

    ?>

</body>


</html>