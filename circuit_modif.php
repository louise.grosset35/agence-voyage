<!DOCTYPE html>

<html lang="fr">
<!-- ouvrir le site depuit le terminal php -S localhost: -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>festival</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css" integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
</head>

<body>
    
    <h1 class = "titre">Modifications :</h1>

    <?php
        include 'config.php';
        $email = $_GET['email'];
        $id_circuit = $_GET['id'];


        $requete_sql = "SELECT user.admin FROM `user` 
        WHERE adresse_mail = :email ;";


                                        
            $data = ['email'=>$email];
            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data); 
            $test_admin = $sth->fetchAll();


        if ($test_admin !=[] and $test_admin[0][0] == 1){
            echo '
            <div class="bouton">
            <a href="circuit_create.php?email=$email"  class="boutons">créer un circuit</a>
            <a href="lieux_show.php?email=$email"  class="boutons">voir les lieux</a>
            <a href="circuit_show.php?email=$email"  class="boutons">voir l"ensemble des circuit</a>

        </div>';
        }       
        
        

            $id_choisi = $_GET['id'];
            $requete_sql = "SELECT * FROM circuit WHERE idCircuit = :id; ";
            $sth = $conexion_bd->prepare($requete_sql);
            $data = ['id' => $id_choisi];
            $is_successful = $sth->execute($data);  
            
            
            $circuit = $sth->fetchAll();
            print_r($circuit);


           
                echo "
                    <div class = 'texte_modif'>
                        <form action='#' method='post'>
                            <h1 class='titre'>Ajustez vos modifications</h1>
              

                                <label for='nom'>Nom du groupe:</label>
                                <input type='text' name='nom' id='nom'  value='".$circuit[0][1]."'>
                                <br /><br />
          
                                <label for='description'>Description:</label>
                                <input type='text' name='description' id='description'  value='".$circuit[0][8]."'>
                                <br /><br />
                       
                     
                                <label for='cout'>Coût:</label>
                                <input type='number' name='prix_inscription' id='prix_inscription'  value='".$circuit[0][6]."'>
                                <br /><br />
                          
                        
                         
                                <label for='pays'>nombre d'etapes:</label>
                                <input type='number' name='nb_etapes' id='nb_etapes'  value='".$circuit[0][4]."'>
                                <br /><br />

                        
                                <label for='email'>duree:</label>
                                <input type='time' name='duree' id='duree'  value='".$circuit[0][5]."'>
                                <br /><br />
               
                                <label for='photos'>ville d'arriver:</label>
                                <input type='number' name='id_ville_A' id='id_ville_A'  value='".$circuit[0][3]."'>
                                <br /><br />

                                <label for='photos'>ville de départ:</label>
                                <input type='number' name='id_ville_D' id='id_ville_D'  value='".$circuit[0][2]."'>
                                <br /><br />

                                <label for='photos'>photos:</label>
                                <input type='text' name='photo' id='photo'  value='".$circuit[0][9]."'>
                                <br /><br />



                            
                            <input class='boutons' type='submit' value='Modifier ce groupe' name='confirmation'>

                        </form>
                    </div>";
            
            if (isset($_POST['confirmation'])){
                echo"<h1 class = 'titre'>votre circuit a été modifier avec succès </h1>";
            
            
            $table = ['nom','id_ville_D','id_ville_A','nb_etapes','duree','prix_inscription','description','photo'];
            $modif = [];
            for ($i = 0; $i <= 7; $i++){
                $valeur =  $_POST[$table[$i]]??$circuit[$i];
                $modif[$i] = $valeur;
            }
            

            $requete_sql = "UPDATE circuit 
            SET nom = :nvnom,
            id_ville_D = $modif[1],
            id_ville_A = $modif[2],
            nb_etapes = $modif[3],
            duree = :duree,
            prix_inscription = $modif[5],
            circuit.description = :nv_desc,
            photos = :nv_photo
            WHERE idCircuit = $id_choisi";

            $data = [
                'nvnom'=>$modif[0],
                'nv_desc' => $modif[6],
                'nv_photo' => $modif[7],
                'duree'=>strval($modif[4])];


            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data);


                
    }?>

</body>
</html>