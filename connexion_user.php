<!DOCTYPE html>

<html lang="fr">
<!-- ouvrir le site depuit le terminal php -S localhost: -->

<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" integrity="sha512-NhSC1YmyruXifcj/KFRWoC561YpHpc5Jtzgvbuzx5VozKpWvQ+4nXhPdFgmx8xqexRcpAglTj9sIBWINXa8x5w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">  
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">

</head>

<body class="bg-index">

    <div class="btn-retour">
        <a href="index.php" class="btn btn-light btn-lg btn-outline-dark">Retour</a>
    </div>
    <div class="btn-retour-tel">
        <a href="index.php" class="btn btn-light btn-outline-dark fw-semibold">Retour</a>
    </div>

    <div class="center-div">
        <form action='#' method='post'>
            <h2 class='text-center bg-light text-wrap around text-uppercase fw-semibold'>Connectez-vous</h2>
                <br /><br />
                <div>
                    <div class =''>
                        <label for='atribut' class="fs-5 fw-semibold black">Adresse mail :</label>
                        <input type='text' class="form-control-sm" name='email' id='email' >
                        <br /><br />
                    </div>
                </div>
                <div>
                    <div class =''>
                        <label for='atribut' class="fs-5 fw-semibold black">Mot De Passe :</label>
                        <input type='text' class="form-control-sm" name='mdp' id='mdp' >
                        <br /><br /><br><br><br>    
                    </div>
                </div>
            <div classe="ok">
                <input type='submit' class='btn bg-light btn-outline-dark'value='Se connecter'>
            </div>
        </form>
        </div>         
    <?php
        include 'config.php';
        if (isset($_POST['email']))  {
            $email = $_POST['email'];
            $mdp = $_POST['mdp'];


            $requete_sql = "SELECT adresse_mail FROM `user` 
                            WHERE adresse_mail = :email ;";

            
                                                            
            $data = ['email'=>$email];
            $sth = $conexion_bd->prepare($requete_sql);
            $is_successful = $sth->execute($data); 
            $test_email = $sth->fetchAll();
            
            if ($test_email != []){
                $requete_sql = "SELECT mdp FROM `user` 
                WHERE adresse_mail = :email ;";
                
                $data = ['email'=>$email];
                $sth = $conexion_bd->prepare($requete_sql);
                $is_successful = $sth->execute($data); 
                $test_mdp = $sth->fetchAll();

                if ($test_email[0][0]== $email and $test_mdp[0][0] == $mdp){
                    echo"
                    <br><br><div class='center-div'><a href='circuit_show.php?email=$email'  class='btn btn-success'>Voir les réservations</a></div>";
                }else {
                    echo "
                    <br><br><h3 class = 'titre'>Mot de Passe invalide</h3>";}}  
                else {
                echo "
                <br><br><div class='center-div'><h3 class = 'titre'>Adresse mail inexistante ou invalide :</h3>
                <a href='creation_user.php'  class='btn btn-success'>Créer un compte</a></div>";}}
        ?>
    
    </body>
    </html>


</body>
</html>